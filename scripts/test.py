#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Simple test for cron jobs etc."""

import datetime
from scripts import login
import sys
import teahouse_archival_bot as thb
import pywikibot

# print(datetime.datetime.utcnow(), ' - test executed')
# print('Python PATH is:', sys.path)
# print('I will attempt to login via PWB')
# login.main()
# print('login.main() did not freeze')

# The following diff, dated 2018-10-15 13:58, was redacted:
# https://en.wikipedia.org/w/index.php?title=Wikipedia:Teahouse&diff=next&oldid=864158358
# didi = thb.get_revisions_from_api('Wikipedia:Teahouse', '2018-10-15T13:50:00Z',
                           # '2018-10-15T14:00:00Z')
# didi = thb.get_sections_from_revid(845926927)
# print(didi)

help(pywikibot.data.api.Request)
